#! /usr/bin/python

import os
import sys
import mouse

try:
    import mido
except:
    print('Python3 lib "mido" is not installed, try to exec "pip install mido"')
    exit(1)
try:
    import rtmidi
except:
    print('Python3 lib "python-rtmidi" is not installed, try to exec "pip install python-rtmidi"')
    exit(1)

try:
    from PyQt5.QtCore import QObject, pyqtSignal
    from PyQt5.QtWidgets import QApplication
    from PyQt5.QtCore import QThread
    from PyQt5.QtWidgets import QWidget, QPushButton, QComboBox, QSpinBox, QGroupBox, \
        QFormLayout, QLabel, QVBoxLayout, QProgressBar, QCheckBox
except:
    print('PyQt5 is not installed, try to install it')
    exit(1)


class SimulRunner(QObject):
    stepIncreased = pyqtSignal(int, name='stepIncreased')

    def __init__(self):
        super(SimulRunner, self).__init__()
        self._step = 0
        self._isRunning = False
        self._maxSteps = 20
        self._knob_value = 0
        self._vertical = False
        self._multiplyer = 10

    def longRunning(self):
        while True:
            QApplication.processEvents()
            if self._isRunning == True:
                r = self.midi_device.receive()
                if r.type == 'control_change' and r.channel == self.channel and r.control == self.control:
                    old_val = self._knob_value
                    new_val = r.value
                    self._knob_value = new_val
                    self.stepIncreased.emit(new_val)
                    # os.system(('pactl set-sink-volume 0 %d' % r.value) + '%')
                    if mouse.is_pressed("left"):
                        upgrade_val = (old_val - new_val) * self._multiplyer
                        if self._vertical:
                            mouse.drag(0, 0, -upgrade_val, 0, absolute=False, duration=0.1)
                        else:
                            mouse.drag(0, 0, 0, upgrade_val, absolute=False, duration=0.1)
                    print("dooo", old_val, new_val)

    def start(self, name, channel, control, vertical, multiplyer):
        self.midi_device = mido.open_ioport(name)
        self.channel = channel
        self.control = control
        self._isRunning = True
        self._vertical = vertical
        self._multiplyer = multiplyer

    def stop(self):
        self._isRunning = False
        self.midi_device.close()


class KnobGui(QWidget):
    def __init__(self):
        super().__init__()
        self.midiout = rtmidi.MidiOut()
        self.port = None
        self.init_ui()
        self.init_background_thread()

    def init_background_thread(self):
        # create
        self.simulRunner = SimulRunner()
        self.simulThread = QThread()
        self.simulRunner.moveToThread(self.simulThread)
        # start and listen
        self.simulThread.start()
        self.simulThread.started.connect(self.simulRunner.longRunning)
        self.simulRunner.stepIncreased.connect(self.knob_update)

    def init_ui(self):
        self.setWindowTitle('Midi Knobs to Master Audio Volume')
        layout = QVBoxLayout()

        connect_form = self.create_connect_form()
        layout.addWidget(connect_form)

        knob_form = self.create_knob_form()
        layout.addWidget(knob_form)

        self.setLayout(layout)
        self.show()

    def create_connect_form(self):
        form_group = QGroupBox("Midi Controller")
        layout = QFormLayout()
        self.cb = QComboBox()
        self.cb.addItems(self.midiout.get_ports())
        layout.addRow(QLabel("Select Device:"), self.cb)

        self.button_connect = QPushButton('Connect', self)
        self.button_connect.setToolTip('Connect to selected midi device')
        self.button_connect.clicked.connect(self.click_connect)

        self.button_disconnect = QPushButton('Disconnect', self)
        self.button_disconnect.setToolTip('Disconnect to midi controller')
        self.button_disconnect.clicked.connect(self.click_disconnect)

        layout.addRow(self.button_connect, self.button_disconnect)

        form_group.setLayout(layout)
        return form_group

    def create_knob_form(self):
        form_group = QGroupBox("Knob")
        layout = QFormLayout()

        self.channel = QSpinBox()
        self.channel.setToolTip('Midi knob channel')
        layout.addRow(QLabel("Channel:"), self.channel)

        self.control = QSpinBox()
        self.control.setToolTip('Midi knob control')
        self.control.setValue(14)
        layout.addRow(QLabel("Control:"), self.control)

        self.vertical = QCheckBox("vertical")
        self.vertical.setCheckState(False)
        layout.addRow(self.vertical)
        
        self.multiplyer = QSpinBox()
        self.multiplyer.setValue(5)

        layout.addRow(QLabel("Multiplyer:"), self.multiplyer)

        self.progress_bar = QProgressBar()
        self.progress_bar.setToolTip('Value')
        self.progress_bar.setRange(0, 127)
        layout.addRow(self.progress_bar)

        form_group.setLayout(layout)
        return form_group

    def click_disconnect(self):
        self.simulRunner.stop()
        self.close()

    def click_connect(self):
        selected_midi_controller = self.cb.currentText()
        channel = int(self.channel.value())
        control = int(self.control.value())
        vertical = bool(self.vertical.checkState())
        multiplyer = int(self.multiplyer.value())
        self.simulRunner.start(selected_midi_controller, channel, control, vertical, multiplyer)

    def knob_update(self, value):
        self.progress_bar.setValue(value)

def main():
    app = QApplication(sys.argv)
    ex = KnobGui()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
