# Midi Knob Facility
This simple application move your mouse up and down when you click something and move the selected knob. I create this because I want to use knobs to manimulate my plugins in my favorite DAW, but I have no enoght knob and set it into every plugin is annoying. So I click on one knob and during the click I use the knob.

![gui](imgs/knob-move-mouse.png)

This do not map the midi knob to the virtual knob but you can use it to up and down the knob.

## How to use

* install requirements
```
python3.8 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

* run:
```
sudo su # because only with sudo you can move the mouse
source venv/bin/activate
python main.py
# Now select your midi controller and the channel and Control numbers of your knob
# Optionally you can chouse if you want vertical movemant instead of vertical default
# You can also modify the multipyer to increase or decrease the mouse translation size
# Click connect (click again connect if you change Control or channel)
# use the knob and whach the moving bar that will be realtime after a bit of moovement
# now go to your daw
# mouse left click on virtual knob
# move the midi knob
# release mouse click
```
